package net.iescierva.ramonmr95.mislugares2019examen.casos_uso;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import net.iescierva.ramonmr95.mislugares2019examen.presentacion.AcercaDeActivity;
import net.iescierva.ramonmr95.mislugares2019examen.presentacion.PreferenciasActivity;
import net.iescierva.ramonmr95.mislugares2019examen.presentacion.VistaLugarActivity;

public class CasosUsoActividades {

    private Activity actividad;

    public CasosUsoActividades(Activity actividad) {
        this.actividad = actividad;
    }

    public void lanzarAcercaDe(View view){
        Intent i = new Intent(actividad, AcercaDeActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarPreferencias(View view){
        Intent i = new Intent(actividad, PreferenciasActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarVistaLugarActividad(View view) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        actividad.startActivity(i);
    }

}
