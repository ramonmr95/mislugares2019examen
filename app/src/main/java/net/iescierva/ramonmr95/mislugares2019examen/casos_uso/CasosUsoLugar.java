package net.iescierva.ramonmr95.mislugares2019examen.casos_uso;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import net.iescierva.ramonmr95.mislugares2019examen.R;
import net.iescierva.ramonmr95.mislugares2019examen.datos.Lugares;
import net.iescierva.ramonmr95.mislugares2019examen.modelo.Lugar;
import net.iescierva.ramonmr95.mislugares2019examen.presentacion.EdicionLugarActivity;
import net.iescierva.ramonmr95.mislugares2019examen.presentacion.VistaLugarActivity;

public class CasosUsoLugar {
    private Activity actividad;
    private Lugares lugares;

    public CasosUsoLugar(Activity actividad, Lugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
    }

    // OPERACIONES BÁSICAS
    public void mostrar(int pos) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void borrar(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle(R.string.borradolugar_title)
                .setMessage(R.string.borradolugar_sum)

                .setPositiveButton(R.string.borradolugar_aceptar_borrar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        lugares.borrar(id);
                        actividad.finish();
                    }})
                .setNegativeButton(R.string.borradolugar_cancelar_borrar, null)
                .show();
    }

    public void guardar(int id, Lugar nuevoLugar) {
        lugares.actualiza(id, nuevoLugar);
    }


    public void editar(int pos, int codidoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codidoSolicitud);
    }

}