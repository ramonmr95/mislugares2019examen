package net.iescierva.ramonmr95.mislugares2019examen.datos;

public class GeoException extends Exception {

    public GeoException(String s) {
        super(s);
    }

}
