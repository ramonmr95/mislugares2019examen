package net.iescierva.ramonmr95.mislugares2019examen.presentacion;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import net.iescierva.ramonmr95.mislugares2019examen.Aplicacion;
import net.iescierva.ramonmr95.mislugares2019examen.R;
import net.iescierva.ramonmr95.mislugares2019examen.casos_uso.CasosUsoActividades;
import net.iescierva.ramonmr95.mislugares2019examen.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.mislugares2019examen.datos.Lugares;

public class MainActivity extends AppCompatActivity {

    private Button bAcercaDe;
    private Button bSalir;
    private Lugares lugares;
    private CasosUsoLugar usoLugar;
    private CasosUsoActividades casosUsoActividades;
    private RecyclerView recyclerView;
    public AdaptadorLugares adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addToolbarAndFloatButton();
        initDatos();
        createRecycleView();
        addRecycleViewListener();
    }

    public void initDatos() {
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this, lugares);
        casosUsoActividades = new CasosUsoActividades(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            casosUsoActividades.lanzarPreferencias(null);
            return true;
        }

        if (id == R.id.acercaDe) {
            casosUsoActividades.lanzarAcercaDe(null);
            return true;
        }

        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void lanzarVistaLugar(View view) {
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle(R.string.vistalugar_lanzar)
                .setMessage(R.string.vistalugar_lanzar_id)
                .setView(entrada)
                .setPositiveButton(R.string.vistalugar_lanzar_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt(entrada.getText().toString());
                        usoLugar.mostrar(id);
                    }
                })
                .setNegativeButton(R.string.vistalugar_lanzar_cancelar, null)
                .show();
    }


    public void cerrar(View view) {
        finish();
    }

    public void createRecycleView() {
        recyclerView = findViewById(R.id.recycler_view);
        adaptador = new AdaptadorLugares(this, lugares);
        recyclerView.setAdapter(adaptador);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        separador = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);
    }

    public void addRecycleViewListener() {
        adaptador.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = recyclerView.getChildAdapterPosition(v);
                usoLugar.mostrar(pos);
            }
        });
    }

    public void addToolbarAndFloatButton() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        adaptador.notifyDataSetChanged();
    }

    //        bAcercaDe = findViewById(R.id.acercaDeButton);
//        bAcercaDe.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                casosUsoActividades.lanzarAcercaDe(null);
//
//            }
//        });
//
//        Button bPreferencias = findViewById(R.id.preferenciasButton);
//        bPreferencias.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                casosUsoActividades.lanzarPreferencias(null);
//            }
//        });


//        bSalir = findViewById(R.id.salirButton);
//        bSalir.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cerrar(null);
//            }
//        });

//    public void mostrarPreferencias(View view){
//        SharedPreferences pref =
//                PreferenceManager.getDefaultSharedPreferences(this);
//        String s = "notificaciones: "+ pref.getBoolean("notificaciones",true)
//                +", máximo a listar: " + pref.getString("maximo","?");
//        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
//    }

    //    public void lanzarAcercaDe(View view){
//        Intent i = new Intent(this, AcercaDeActivity.class);
//        startActivity(i);
//    }

    //    public void lanzarPreferencias(View view){
//        Intent i = new Intent(this, PreferenciasActivity.class);
//        startActivity(i);
//    }

}
