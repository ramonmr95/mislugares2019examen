package net.iescierva.ramonmr95.mislugares2019examen;

import android.app.Application;

import net.iescierva.ramonmr95.mislugares2019examen.datos.Lugares;
import net.iescierva.ramonmr95.mislugares2019examen.datos.LugaresLista;
import net.iescierva.ramonmr95.mislugares2019examen.presentacion.AdaptadorLugares;

public class Aplicacion extends Application {

    public Lugares lugares;
    public AdaptadorLugares adaptador;

    public Aplicacion() {
        lugares = new LugaresLista();
        adaptador = new AdaptadorLugares(lugares);

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Lugares getLugares() {
        return lugares;
    }
}