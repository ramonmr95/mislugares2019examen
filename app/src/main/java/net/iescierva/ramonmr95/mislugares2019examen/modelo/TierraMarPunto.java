package net.iescierva.ramonmr95.mislugares2019examen.modelo;

import net.iescierva.ramonmr95.mislugares2019examen.datos.GeoException;

public class TierraMarPunto extends GeoPuntoAlt {

    private double altura;
    private double profundidad;

    public TierraMarPunto(double latitud, double longitud, double altura) throws GeoException {
        super(latitud, longitud, altura);
        this.altura = altura;
    }

    public TierraMarPunto(double latitud, double longitud, double altura, double profundidad) throws GeoException {
        super(latitud, longitud, altura);
        this.altura = altura;
        this.profundidad = profundidad;
    }


    @Override
    public boolean isAltitudOk() {
        if (altura > 0) {
            return super.isAltitudOk();
        }
        else {
            return isProfundidadOk(profundidad);
        }
    }

    public boolean isProfundidadOk(double profundidad) {
        return profundidad >= -11000 && profundidad <= 0;
    }
}
